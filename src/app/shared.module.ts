import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BeerDialogComponent } from './components/beer-dialog/beer-dialog.component';
import { BeerItemComponent } from './components/beer-item/beer-item.component';
import { HeaderComponent } from './components/header/header.component';
import { SearchComponent } from './components/search/search.component';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { BeerListComponent } from './components/beer-list/beer-list.component';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule, MatInputModule,
  MatNativeDateModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {NouisliderModule} from 'ng2-nouislider';

@NgModule({
  entryComponents: [
    BeerDialogComponent
  ],
  declarations: [
    BeerDialogComponent,
    BeerItemComponent,
    HeaderComponent,
    SearchComponent,
    BeerListComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatCardModule,
    MatDialogModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatInputModule,

    FormsModule,
    NouisliderModule
  ],
  exports: [
    BeerDialogComponent,
    BeerItemComponent,
    HeaderComponent,
    SearchComponent,
    BeerListComponent,
    HttpClientModule,
    MatCardModule,
    MatDialogModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatInputModule,
    NouisliderModule
  ]
})
export class SharedModule { }
