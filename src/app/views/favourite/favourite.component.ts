import { Component, OnInit } from '@angular/core';
import {FavouritesService} from '../../services/favourites.service';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.scss']
})
export class FavouriteComponent implements OnInit {

  constructor(public favouritesService: FavouritesService) { }

  ngOnInit(): void {
  }

}
