import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavouriteComponent } from './favourite.component';
import {SharedModule} from '../../shared.module';

@NgModule({
  declarations: [FavouriteComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class FavouriteModule { }
