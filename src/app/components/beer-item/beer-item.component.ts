import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IBeer} from '../../models/beer.model';
import {FavouritesService} from '../../services/favourites.service';
import {BeerDialogComponent} from '../beer-dialog/beer-dialog.component';
import {MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-beer-item',
  templateUrl: './beer-item.component.html',
  styleUrls: ['./beer-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BeerItemComponent implements OnInit {
  @Input() beer: IBeer;
  @Input() returnBeerAfterClick = false;
  @Input() small = false;
  @Output() beerClicked: EventEmitter<IBeer> = new EventEmitter<IBeer>();

  private dialogRef: MatDialogRef<BeerDialogComponent>;

  constructor(public favouritesService: FavouritesService,
              private dialog: MatDialog,
              private ref: ChangeDetectorRef) { }

  ngOnInit() {}

  toggleFavourite(event, beer: IBeer): void {
    event.preventDefault();
    event.stopPropagation();

    this.favouritesService.toggle(beer);
    this.ref.markForCheck();
  }

  openBeerDialog(beer: IBeer): void {
    if (this.returnBeerAfterClick) {
      this.beerClicked.emit(beer);
      return;
    }
    this.dialogRef = this.dialog.open(BeerDialogComponent, {
      data: beer,
      height: '95%',
      width: '60%',
      panelClass: 'beer-dialog'
    });
  }
}
