import {Component, Inject, OnInit} from '@angular/core';
import {IBeer} from '../../models/beer.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {BeerRepositoryService, IBeerFilters} from '../../services/beer.repository.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-beer-dialog',
  templateUrl: './beer-dialog.component.html',
  styleUrls: ['./beer-dialog.component.scss']
})
export class BeerDialogComponent implements OnInit {

  private IBU_SIMILAR_THRESHOLD = 4;
  private SIMILAR_BEERS_AMOUNT = 3;
  public $similarBeers: Observable<IBeer[]>;

  constructor(
    private beerRepositoryService: BeerRepositoryService,
    public dialogRef: MatDialogRef<BeerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public beer: IBeer) {
  }

  ngOnInit(): void {
    this.fetchSimilarBeers();

  }

  close(): void {
    this.dialogRef.close();
  }

  reloadView(beer: IBeer): void {
    this.beer = beer;
    this.fetchSimilarBeers();
  }

  fetchSimilarBeers(): void {
    const filters: IBeerFilters = {
      page: 1,
      per_page: this.SIMILAR_BEERS_AMOUNT + 1,
      ibu_gt: this.clipNumber(Math.floor(this.beer.ibu - this.IBU_SIMILAR_THRESHOLD), 0),
      ibu_lt: this.clipNumber(Math.ceil(this.beer.ibu + this.IBU_SIMILAR_THRESHOLD), 0),
    };

    this.$similarBeers = this.beerRepositoryService.getList(filters).pipe(
      map(beers => beers.filter(beer => beer.id !== this.beer.id).slice(0, this.SIMILAR_BEERS_AMOUNT))
    );
  }

  clipNumber(number, min, max = 9999): number {
    return Math.max(min, Math.min(number, max));
  }
}


