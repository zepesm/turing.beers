import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeerDialogComponent } from './beer-dialog.component';
import {MAT_DIALOG_DATA, MatCardModule, MatDialogModule, MatDialogRef} from '@angular/material';
import {BeerItemComponent} from '../beer-item/beer-item.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('BeerDialogComponent', () => {
  let component: BeerDialogComponent;
  let fixture: ComponentFixture<BeerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeerDialogComponent, BeerItemComponent ],
      imports: [MatDialogModule, MatCardModule, HttpClientTestingModule],
      providers: [{provide : MatDialogRef, useValue : {}}, { provide: MAT_DIALOG_DATA, useValue: {}}]
    })
    .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(BeerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
