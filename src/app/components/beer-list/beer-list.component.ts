import {Component, HostListener, Input, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {IBeer} from '../../models/beer.model';
import {BeerRepositoryService, IBeerFilters} from '../../services/beer.repository.service';
import {StateService} from '../../services/state.service';
import {FavouritesService} from '../../services/favourites.service';
import {debounce} from '../../app.decorators';

@Component({
  selector: 'app-beer-list',
  templateUrl: './beer-list.component.html',
  styleUrls: ['./beer-list.component.scss']
})
export class BeerListComponent implements OnInit, OnDestroy {
  @Input() favourites: number[];
  @Input() infinite = false;

  private BATCH_FETCH_AMOUNT = 10;
  private filters: IBeerFilters = {
    per_page: this.BATCH_FETCH_AMOUNT,
    page: 1
  };
  private $query: Subscription;
  private $favourites: Subscription;
  private fetchedAllBeers = false;
  public $beers: Subscription;
  public beers: IBeer[] = [];

  constructor(private beerRepository: BeerRepositoryService,
              private favouritesService: FavouritesService,
              private stateService: StateService) {
  }

  ngOnInit(): void {
    if (this.favourites) {
      if (!this.favourites.length) {
        return;
      }
      this.filters.ids = this.favourites.join('|');
    }

    this.fetchBeers();

    this.$query = this.stateService.onSearchQueryChange.subscribe(query => {
      this.filters.page = 1;
      this.fetchedAllBeers = false;
      this.beers = [];
      this.filters.beer_name = query ? query : null;
      this.fetchBeers();
    });

    this.$query = this.stateService.onAdditionalFiltersChange.subscribe(query => {
      this.filters.page = 1;
      this.beers = [];
      this.fetchedAllBeers = false;
      this.fetchBeers();
    });

    this.$favourites = this.favouritesService.onFavouriesChange.subscribe(favourites => {
      if (this.favourites) {
        const currentFiltersLength = this.filters.ids ? this.filters.ids.length : '';
        this.filters.ids = favourites.join('|');
        if (this.filters.ids.length > currentFiltersLength) {
          this.filters.page = 1;
          this.fetchedAllBeers = false;
          this.beers = [];
          this.fetchBeers();
        }
      }
    });
  }

  private fetchBeers(): void {
    this.$beers = this.beerRepository.getList({...this.filters, ...this.stateService.additionalFilters}).subscribe(beers => {
      this.processBeerListResponse(beers);
    });
  }

  private processBeerListResponse(beers): void {
    if (!beers.length) {
      this.fetchedAllBeers = true;
      return;
    }


    this.beers = this.favourites ? beers : this.beers.concat(beers);
    this.filters.page++;
  }


  @HostListener('window:scroll', [])
  @debounce()
  onScroll(): void {
    if (this.favourites || this.fetchedAllBeers || !this.infinite) {
      return;
    }
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - 200) {
      this.fetchBeers();
    }
  }


  ngOnDestroy(): void {
    if (this.$query) {
      this.$query.unsubscribe();
    }

    if (this.$beers) {
      this.$query.unsubscribe();
    }
    if (this.$favourites) {
      this.$favourites.unsubscribe();
    }
  }

}
