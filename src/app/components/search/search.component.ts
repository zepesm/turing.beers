import {Component, OnInit, ViewChild} from '@angular/core';
import {debounce} from '../../app.decorators';
import {StateService} from '../../services/state.service';
import {MatDatepickerInputEvent, MatInput} from '@angular/material';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @ViewChild('pickerAfterInput', { read: MatInput}) inputAfter: MatInput;
  @ViewChild('pickerBeforeInput', { read: MatInput}) inputBefore: MatInput;

  private MAX_ABV = 60;
  private MAX_IBU = 200;
  private MAX_EBC = 100;

  private RANGE_STEPS = 10;

  public query: string;
  public advSearchExpanded = false;
  public selectedRanges = {
    abv: [0, this.MAX_ABV],
    ibu: [0, this.MAX_IBU],
    ebc: [0, this.MAX_EBC]
  };

  public rangeConfigs = {
    abv: {
      behaviour: 'drag',
      connect: true,
      start: this.selectedRanges.abv,
      keyboard: true,
      step: 1,
      pageSteps: this.RANGE_STEPS,
      range: {
        min: 0,
        max: this.MAX_ABV
      },
    },
    ibu: {
      behaviour: 'drag',
      connect: true,
      start: this.selectedRanges.ibu,
      keyboard: true,
      step: 1,
      pageSteps: this.RANGE_STEPS,
      range: {
        min: 0,
        max: this.MAX_IBU
      }
    },
    ebc: {
      behaviour: 'drag',
      connect: true,
      start: this.selectedRanges.ebc,
      keyboard: true,
      step: 1,
      pageSteps: this.RANGE_STEPS,
      range: {
        min: 0,
        max: this.MAX_EBC
      }
    }
  };

  constructor(private stateService: StateService) {
  }

  ngOnInit(): void {

  }

  @debounce()
  filtersChanged(event, type = null): void {
    if (event instanceof MatDatepickerInputEvent && type) {
      this.stateService.additionalFilters[type] = event.value ? this.formatDate(event.value) : null;
    }
    this.stateService.additionalFilters.abv_gt = this.selectedRanges.abv[0];
    this.stateService.additionalFilters.abv_lt = this.selectedRanges.abv[1];
    this.stateService.additionalFilters.ibu_gt = this.selectedRanges.ibu[0];
    this.stateService.additionalFilters.ibu_lt = this.selectedRanges.ibu[1];
    this.stateService.additionalFilters.ebc_gt = this.selectedRanges.ebc[0];
    this.stateService.additionalFilters.ebc_lt = this.selectedRanges.ebc[1];
    this.stateService.onAdditionalFiltersChange.next(true);
  }

  resetFilters(): void {
    this.selectedRanges = {
      abv: [0, this.MAX_ABV],
      ibu: [0, this.MAX_IBU],
      ebc: [0, this.MAX_EBC]
    };

    this.inputAfter.value = '';
    this.inputBefore.value = '';
    this.stateService.additionalFilters.brewed_after = null;
    this.stateService.additionalFilters.brewed_before = null;
    this.stateService.onAdditionalFiltersChange.next(true);
  }

  @debounce()
  onQueryChange(event): void {
    this.stateService.onSearchQueryChange.next(event);
  }

  formatDate(date: Date): string {
    if (!date || !(date instanceof Date)) {
      return null;
    }
    const year = date.getFullYear();
    let month = '' + (date.getMonth() + 1);
    month = month.length === 1 ? '0' + month : month;
    return month + '-' + year;
  }

}
