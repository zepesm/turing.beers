import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import {FormsModule} from '@angular/forms';
import {
  MatButtonModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NouisliderModule} from 'ng2-nouislider';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule,
        MatExpansionModule,
        BrowserAnimationsModule,
        NouisliderModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatButtonModule,
        MatInputModule


      ],
      declarations: [ SearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render "Search for beers here" in input placeholder', () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('input').getAttribute('placeholder')).toEqual('Search for beers here');
  });

  it('formatDate: should format date to MM-YYY', () => {
    expect(component.formatDate(new Date(1337, 6, 3))).toEqual('07-1337');
  });

  it('formatDate: should return null on bad date', () => {
    expect(component.formatDate('iddqd' as unknown as Date)).toEqual(null);
  });
});
