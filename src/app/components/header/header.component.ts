import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {Const} from '../../app.config';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @ViewChild('headerGap') headerGap: ElementRef;

  public appConst = Const;
  public stickHeader = false;

  constructor() { }

  ngOnInit(): void {}

  @HostListener('window:scroll', [])
  onScroll(): void {
    this.stickHeader = this.headerGap.nativeElement.offsetTop - window.scrollY < 0;
  }

}
