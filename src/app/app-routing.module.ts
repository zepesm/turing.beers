import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {Const} from './app.config';
import {HomeComponent} from './views/home/home.component';
import {FavouriteComponent} from './views/favourite/favourite.component';

const routes: Routes = [
  {
    path: Const.PATHNAME_HOME,
    component: HomeComponent,
  },
  {
    path: Const.PATHNAME_FAVOURITES,
    component: FavouriteComponent,

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
