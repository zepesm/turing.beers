import {EventEmitter, Injectable} from '@angular/core';
import {IBeerFilters} from './beer.repository.service';


@Injectable({
  providedIn: 'root'
})
export class StateService {

  public onSearchQueryChange: EventEmitter<string> = new EventEmitter<string>();
  public onAdditionalFiltersChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  public additionalFilters: IBeerFilters = {};

  constructor() { }


}
