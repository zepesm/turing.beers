import { TestBed } from '@angular/core/testing';

import { BeerRepositoryService } from './beer.repository.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('Beer.RepositoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [BeerRepositoryService],
    imports: [
      HttpClientTestingModule
    ],
  }));

  it('should be created', () => {
    const service: BeerRepositoryService = TestBed.get(BeerRepositoryService);
    expect(service).toBeTruthy();
  });

});
