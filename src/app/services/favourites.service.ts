import {EventEmitter, Injectable} from '@angular/core';
import {IBeer} from '../models/beer.model';

@Injectable({
  providedIn: 'root'
})
export class FavouritesService {

  public favourites: number[] = [];
  public onFavouriesChange: EventEmitter<number[]> = new EventEmitter<number[]>();

  constructor() {
    this.getPersistedFavourites();
  }

  toggle(beer: IBeer): void {
    if (this.favourites.indexOf(beer.id) !== -1) {
      this.favourites = this.favourites.filter(el => el !== beer.id);
    } else {
      this.favourites.push(beer.id);
    }
    this.onFavouriesChange.emit(this.favourites);
    this.persistFavourites();
  }

  isFavourite(beer: IBeer): boolean {
    return this.favourites.indexOf(beer.id) !== -1;
  }

  persistFavourites(): void {
    window.localStorage.setItem('TRNG_FAVS', JSON.stringify({data: this.favourites}));
  }

  getPersistedFavourites(): void {
    let favs;
    try {
      favs = JSON.parse(window.localStorage.getItem('TRNG_FAVS'));
      if (favs && favs.data) {
        this.favourites = favs.data;
      }
    } catch (e) {}
  }
}
