import { TestBed } from '@angular/core/testing';


import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AbstractRepositoryService} from './_abstract.repository.service';

describe('AbstractRepositoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [AbstractRepositoryService],
    imports: [
      HttpClientTestingModule
    ],
  }));

  it('should be created', () => {
    const service: AbstractRepositoryService<any> = TestBed.get(AbstractRepositoryService);
    expect(service).toBeTruthy();
  });

  it('serializeFilters: should serialize filters', () => {
    const service: AbstractRepositoryService<any> = TestBed.get(AbstractRepositoryService);
    expect(AbstractRepositoryService.serializeFilters({one: '1', two: '2'})).toEqual('?one=1&two=2');
  });

  it('serializeFilters: should omit null on serialize filters', () => {
    const service: AbstractRepositoryService<any> = TestBed.get(AbstractRepositoryService);
    expect(AbstractRepositoryService.serializeFilters({one: '1', two: null})).toEqual('?one=1');
  });
});
