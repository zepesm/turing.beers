import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AbstractRepositoryService<M> {

  endpoint: string;

  public static serializeFilters(filters = {}): string {
    const str = [];
    for (const p in filters) {
      if (filters.hasOwnProperty(p) && filters[p]) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(filters[p]));
      }
    }
    return '?' + str.join('&');
  }


  constructor(private http: HttpClient) { }

  getList(filters = {}): Observable<M[]> {
    return this.http.get<M[]>(environment.baseAPIUrl + this.endpoint + AbstractRepositoryService.serializeFilters(filters));
  }

  getOne(id: number): Observable<M> {
    return this.http.get<M>(environment.baseAPIUrl + this.endpoint + '/' + id);
  }
}
