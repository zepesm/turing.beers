import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AbstractRepositoryService} from './_abstract.repository.service';
import {IBeer} from '../models/beer.model';

export interface IBeerFilters {
  ibu_lt?: number;
  ibu_gt?: number;
  abv_gt?: number;
  abv_lt?: number;
  ebc_gt?: number;
  ebc_lt?: number;
  brewed_before?: string;
  brewed_after?: string;
  beer_name?: string;
  ids?: string;
  per_page?: number;
  page?: number;
}

@Injectable({
  providedIn: 'root'
})

export class BeerRepositoryService extends AbstractRepositoryService<IBeer> {

  endpoint = '/beers';

  constructor(http: HttpClient) {
    super(http);
  }
}

